<?php

namespace DWES\core\exceptions;

use Exception;

class ValidationException extends Exception
{
}