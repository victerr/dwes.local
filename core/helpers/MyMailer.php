<?php


namespace DWES\core\helpers;
use DWES\core\App;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class MyMailer
{
    private $transport;
    private $mailer;

    /**
     * MyMailer constructor.
     */
    public function __construct()
    {
        $config = App::get('config')['mailer'];

        $this->transport = (new Swift_SmtpTransport($config['host'], $config['port']))
            ->setUsername($config['username'])
            ->setPassword($config['password']);

        $this->mailer = new Swift_Mailer($this->transport);
    }

    /**
     * @param string $message
     */
    public function sendEmail(string $user)
    {
        $message = (new Swift_Message('Se ha eliminado un usuario'))
            ->setFrom(['502b46d7d5-d836f6@inbox.mailtrap.io' => 'Papito Remolino'])
            ->setTo(['ivorra.alberola@gmail.com' => 'Vikings'])
            ->setBody('Se ha eliminado el usuario '.$user);
        $this->mailer->send($message);
    }
}