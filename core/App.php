<?php

namespace DWES\core;

use DWES\core\database\Connection;
use DWES\core\exceptions\AppException;
use PDO;

class App
{
    /**
     * @var array
     */
    private static $container = [];

    /**
     * @param string $key
     * @param $value
     * @throws AppException
     */
    public static function bind(string $key, $value)
    {
        if (array_key_exists($key, self::$container))
            throw new AppException("Ya existe el servicio $key en el contenedor de servicios");

        self::$container[$key] = $value;
    }

    /**
     * @param string $key
     * @return mixed
     * @throws AppException
     */
    public static function get(string $key)
    {
        if (!array_key_exists($key, self::$container))
            throw new AppException("No existe el servicio $key en el contenedor de servicios");

        return self::$container[$key];
    }

    /**
     * @param string $key
     * @return mixed
     * @throws AppException
     */
    public static function getService(string $className)
    {
        if (!array_key_exists($className, self::$container))
            self::$container[$className] = new $className();

        return self::$container[$className];
    }

    /**
     * @return PDO
     * @throws AppException
     * @throws QueryException
     */
    public static function getConnection() : PDO
    {
        if (!array_key_exists('connection', self::$container))
            self::$container['connection'] = Connection::make();

        return self::$container['connection'];
    }

    public static function getRepository(string $repositoryClass)
    {
        if (!array_key_exists('repository', self::$container))
            self::$container['repository'] = [];

        if (!array_key_exists($repositoryClass, self::$container['repository']))
            self::$container['repository'][$repositoryClass] = new $repositoryClass();

        return self::$container['repository'][$repositoryClass];
    }
}