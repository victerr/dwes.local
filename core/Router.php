<?php

namespace DWES\core;

use DWES\core\exceptions\AppException;

class Router
{
    private $routes = [
        'GET' => [],
        'POST' => []
    ];

    public static function load(string $file)
    {
        $router = new static;
        require $file;
        return $router;
    }

    /**
     * @param string $uri
     * @param string $controller
     */
    public function get(string $uri, string $controller)
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * @param string $uri
     * @param string $controller
     */
    public function post(string $uri, string $controller) {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * @param string $controller
     * @param string $action
     * @return mixed
     * @throws AppException
     */
    private function callAction(string $controller, string $action)
    {
        $controller = "DWES\\app\\controllers\\$controller";

        $objController = new $controller;

        if(! method_exists($objController, $action))
        {
            throw new AppException(
                "El controlador $controller no responde al action $action");
        }
        return $objController->$action();
    }

    /**
     * @param string $uri
     * @param string $method
     * @return mixed
     * @throws AppException
     */
    public function direct(string $uri, string $method)
    {
        if (array_key_exists($uri, $this->routes[$method]))
        {
            list($controller, $action) = explode('@', $this->routes[$method][$uri]);
            return $this->callAction($controller, $action);
        }

        throw new AppException('No se ha definido una ruta para esta URI');
    }

    public function redirect(string $path)
    {
        header('location: /' . $path);
        exit;
    }
}