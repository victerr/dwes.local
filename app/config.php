<?php

return [
    'database' => [
        'dbname' => 'agenda',
        'dbuser' => 'useragenda',
        'dbpassword' => 'useragenda',
        'connection' => 'mysql:host=dwes.local',
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_PERSISTENT => true
        ]
    ],
    'mailer' => [
        'host' => 'smtp.mailtrap.io',
        'port' => 465,
        'username' => 'bef0b18f2b87cd',
        'password' => '53131e152b939d'
    ],
    'logger' => [
        'directory' => __DIR__.'/logs/app.log'
    ]
];