<?php include __DIR__ . '/partials/cabecera.part.php'; ?>
<div class="container">
    <?php include __DIR__ . '/partials/mensaje.part.php'; ?>
    <div class="row">
        <form method="post" action="/grupos/nuevo" enctype="multipart/form-data">
            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre">
            <input type="submit" value="Enviar" name="enviar">
        </form>
    </div>
    <?php include __DIR__ . '/partials/error.part.php'; ?>
    <div class="row">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Operaciones</th>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Núm. Contactos</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($grupos as $grupo ) : ?>
                <tr>
                    <td>
                        <div class="btn-group" role="group" aria-label="Operaciones">
                            <a href="#" class="btn btn-secondary"><i class="fa fa-edit"></i></a>
                            <a href="/grupos/eliminar?id=<?= $grupo->getId() ?>" class="btn btn-secondary"><i class="fa fa-trash"></i></a>
                        </div>
                    </td>
                    <td><?= $grupo->getId() ?></td>
                    <td><?= $grupo->getNombre() ?></td>
                    <td><?= $grupo->getNumContactos() ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<?php include __DIR__ . '/partials/pie.part.php'; ?>
