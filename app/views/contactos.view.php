<?php include __DIR__ . '/partials/cabecera.part.php'; ?>
<div class="container">
    <?php include __DIR__ . '/partials/mensaje.part.php'; ?>
    <div class="row">
        <form method="post" action="/contactos/nuevo" enctype="multipart/form-data">
            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre">
            <label for="telefono">Teléfono:</label>
            <input type="text" name="telefono">
            <label for="grupo">Grupo:</label>
            <select name="grupo">
                <?php foreach ($grupos as $grupo) : ?>
                    <option value="<?= $grupo->getId() ?>"><?= $grupo->getNombre() ?></option>
                <?php endforeach; ?>
            </select>
            <label for="foto">Foto:</label>
            <input type="file" name="foto">
            <input type="submit" value="Enviar" name="enviar">
        </form>
    </div>
    <?php include __DIR__ . '/partials/error.part.php'; ?>
    <div class="row">
        <?php if (!is_null($id)) :?>
            <form action="/contactos/editar" method="post">
        <?php endif; ?>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Operaciones</th>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Fecha alta</th>
                <th scope="col">Foto</th>
                <th scope="col">Grupo</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($contactos as $contacto ) : ?>
                <tr>
                <?php if ($id == $contacto->getId()) :?>
                    <td>
                        <div class="btn-group" role="group" aria-label="Operaciones">
                            <button class="btn btn-secondary"><i class="fa fa-save"></i></button>
                            <a href="/contactos/editar" class="btn btn-secondary"><i class="fa fa-times"></i></a>
                        </div>
                    </td>
                    <td>
                        <input type="hidden" name="id" value="<?= $contacto->getId() ?>">
                        <input type="text" name="id-disabled" disabled value="<?= $contacto->getId() ?>">
                    </td>
                    <td><input type="text" name="nombre" value="<?= $contacto->getNombre() ?>"></td>
                    <td><input type="text" name="telefono" value="<?= $contacto->getTelefono() ?>"></td>
                    <td><?= $contacto->getFechaAltaFormateada() ?></td>
                    <td><img width="80" src="<?= $contacto->getFoto() ?>" alt="<?= $contacto->getFoto() ?>"></td>
                    <td>
                        <select name="grupo">
                            <?php foreach ($grupos as $grupo) : ?>
                                <option value="<?= $grupo->getId() ?>" <?= ($grupo->getId() === $contacto->getGrupo()) ? 'selected' : '' ?>><?= $grupo->getNombre() ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                <?php else: ?>
                    <td>
                        <div class="btn-group" role="group" aria-label="Operaciones">
                            <a href="/contactos?id=<?= $contacto->getId() ?>" class="btn btn-secondary"><i class="fa fa-edit"></i></a>
                            <a href="/contactos/eliminar?id=<?= $contacto->getId() ?>" class="btn btn-secondary"><i class="fa fa-trash"></i></a>
                        </div>
                    </td>
                    <td><?= $contacto->getId() ?></td>
                    <td><?= $contacto->getNombre() ?></td>
                    <td><?= $contacto->getTelefono() ?></td>
                    <td><?= $contacto->getFechaAltaFormateada() ?></td>
                    <td><img width="80" src="<?= $contacto->getFoto() ?>" alt="<?= $contacto->getFoto() ?>"></td>
                    <td><?= $contactoRepository->getGrupo($contacto)->getNombre() ?></td>
                <?php endif; ?>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php if (!is_null($id)) :?>
            </form>
        <?php endif; ?>
    </div>
</div>
<?php include __DIR__ . '/partials/pie.part.php'; ?>
