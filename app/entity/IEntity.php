<?php

namespace DWES\app\entity;

interface IEntity
{
    public function toArray();
}