<?php

namespace DWES\app\utils;

class Utils
{
    public static function isCurrentPage(string $pageName) : bool
    {
        if (strpos($_SERVER['REQUEST_URI'], $pageName) !== false)
            return true;

        return false;
    }
}