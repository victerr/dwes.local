<?php

namespace DWES\app\controllers;

use DWES\app\entity\Contacto;
use DWES\app\repository\ContactoRepository;
use DWES\app\repository\GrupoRepository;
use DWES\app\utils\File;
use DWES\core\App;
use DWES\core\exceptions\ValidationException;
use DWES\core\helpers\MyLogger;
use DWES\core\helpers\MyMailer;

class ContactoController
{
    public function listar()
    {
        $id = $_GET['id'] ?? null;
        $grupos = App::getRepository(GrupoRepository::class)->findAll();

        $contactoRepository = App::getRepository(ContactoRepository::class);
        $contactos = $contactoRepository->findAll();

        include __DIR__ . '/../views/contactos.view.php';
    }

    public function nuevo()
    {
        if (!isset($_POST['nombre']) || empty($_POST['nombre']))
            throw new ValidationException('El campo nombre no se puede quedar vacío');
        if(!isset($_POST['telefono']) || empty($_POST['telefono']))
            throw new ValidationException('El campo teléfono no se puede quedar vacío');
        if(!isset($_POST['grupo']) || empty($_POST['grupo']))
            throw new ValidationException('El campo grupo no se puede quedar vacío');

        $nombre = $_POST['nombre'];
        $telefono = $_POST['telefono'];
        $grupo = $_POST['grupo'];

        $contacto = new Contacto();
        $contacto->setNombre($nombre);
        $contacto->setTelefono($telefono);
        $contacto->setGrupo($grupo);

        $file = new File(
            'foto',
            'uploads/',
            ['image/jpeg', 'image/png']
        );

        $file->uploadFile();

        $foto = $file->getFileUrl();

        $contacto->setFoto($foto);

        App::getRepository(ContactoRepository::class)->nuevo($contacto);

        $mensaje = "Se ha insertado correctamente el contacto con nombre $nombre";
        App::getService(MyLogger::class)->addMessage($mensaje);

        App::get('router')->redirect('contactos');
    }

    public function editar()
    {
        if (!isset($_POST['nombre']) || empty($_POST['nombre']))
            throw new ValidationException('El campo nombre no se puede quedar vacío');
        if(!isset($_POST['telefono']) || empty($_POST['telefono']))
            throw new ValidationException('El campo teléfono no se puede quedar vacío');
        if(!isset($_POST['grupo']) || empty($_POST['grupo']))
            throw new ValidationException('El campo grupo no se puede quedar vacío');

        $id = $_POST['id'];
        /**
         * @var Contacto $contacto
         */
        $contacto = App::getRepository(ContactoRepository::class)->find($id);

        $nombre = $_POST['nombre'];
        $telefono = $_POST['telefono'];
        $grupo = $_POST['grupo'];
        $idGrupoAnterior = $contacto->getGrupo();

        $contacto->setNombre($nombre);
        $contacto->setTelefono($telefono);
        $contacto->setGrupo($grupo);

        App::getRepository(ContactoRepository::class)->edita($contacto, $idGrupoAnterior);

        $mensaje = "El contacto se ha editado correctamente";

        App::get('router')->redirect('contactos');
    }

    public function eliminar()
    {
        if (isset($_GET['id']) && !empty($_GET['id'])) {
            $contactoRepository = App::getRepository(ContactoRepository::class);

            $id = $_GET['id'];
            $contacto = $contactoRepository->find($id);
            $contactoRepository->elimina($contacto);
            App::getService(MyMailer::class)->sendEmail($contacto->getNombre());

            App::get('router')->redirect('contactos');
        }
    }
}